////////////////////////////////////////////////////////////////////////////////
//          curso_verilog.v - Practical introductory Verilog course           //
//                    Jorge Juan-Chico <jjchico@gmail.com>                    //
//                                                                            //
// This file is part of curso_verilog.v. curso_verilog.v is free software:    //
// you can redistribute it and/or modify it under the terms of the GNU        //
// General Public License as published by the Free Software Foundation,       //
// either version 3 of the License, or (at your option) any later version.    //
// See <http://www.gnu.org/licenses/>.                                        //
////////////////////////////////////////////////////////////////////////////////