# verilog-course.v

**NOTE: THIS IS WORK IN PROGRESS. YOU MAY FIND UNFINISHED FILES AND/OR PARTS
IN THE ORIGINAL SPANISH VERSION. STAY TUNNED.**

## IN OTHER LANGUAGES

* [Español (Spanish)](https://gitlab.com/jjchico/curso-verilog.v)

## INTRODUCTION

Hi!

This is a basic Verilog course completely written in Verilog language. Other
courses include examples but this one is more a collection of examples
containing a course. Every Verilog file ('.v' extension) is a Verilog coding
example together with comments and brief conceptual descriptions.

The course is divided in units and lessons. The overall structure follows a
traditional digital circuits design course. Some basic digital design concepts
are briefly explained, but it is assumed that the student is familiar with basic
digital design concepts or is studying these along this course.

This course focuses on the Verilog language itself and does not does not go down
to the implementation of the examples. However, all the examples could be
synthesized using any standard Verilog-compatible digital circuits synthesis
tool. Today, it is easy and affordable to implement digital designs on FPGA
devices using free (open source) tools like
[Icestorm](https://github.com/cliffordwolf/icestorm),
[Apio](https://github.com/FPGAwars/apio) or
[Icestudio](https://github.com/FPGAwars/icestudio)

## HOW TO START

To do the course you only need to clone or dowload this repository and go into
the *verilog* folder. In this folder you can find subfolders with units and
lessons sorted numerically. Simply open the files in order and follow the
instructions in the comments that go with the code.

## TOOLS

To follow this course you will need to compile and simulate the proposed
examples. This course uses [Icarus Verilog](http://www.icarus.com/eda/verilog/)
as simulator and [Gtkwave](http://gtkwave.sourceforge.net/) as waveform viewer.
All lessons include detailed instructions to simulate the examples and see the
results using these tools. This course assumes that the user has the tools
installed and is able to execute them from the command line of the operating
system. However, any set of tools that allows editing text files and simulating
Verilog code can be used as an alternative, including FPGA's manufacturers
integrated environments and web-based platforms like
[EDAPlayground](https://www.edaplayground.com/).

Both Icarus Verilog and Gtkwave are available in the software repositories of
major Linux repositories, thus Linux is an ideal platform to follow this course.
The default text editor in any Linux distribution can be used to edit the code.
In Debian-based Linux distributions like Ubuntu you can install the necessary
programs from a terminal with:

    $ sudo apt install iverilog gtkwave

Icarus Verilog and Gtkwave are also available for [Microsoft
Windows(TM)](http://bleyer.org/icarus/). The author does not know if this option
is up to date or actively maintained. To edit Verilog code you can use any plain
text editor compatible with UNIX text files and UTF8 encoding. MS Windows user
will probably prefer an advanced text editor like
[Notepad++](http://notepad-plus-plus.org/) instead of the system's default text
editor.

Being native GNU/Linux programs, Icarus Verilog and Gtkwave are likely to be
available in other UNIX-like operating systems. The interested user should
explore these options.

## CONTENTS

### Unit 1. Hardware description languages fundamentals

  * Lesson 1-1. Introduction to hardware description languages
  * Lesson 1-2. Combinational functions description and simulation
  * Lesson 1-3. Types of descriptions in Verilog

### Unit 2. Test bench

  * Lesson 2-1. Simulation with a test bench
  * Lesson 2-2. Descriptions comparison with the same test bench

### Unit 3. Combinational examples

  * Lesson 3-1. Simple automobile alarm system
  * Lesson 3-2. Prime number detector
  * Lesson 3-3. Delay and hazards demonstrator

### Unit 4. Combinational subsystems

  * Lesson 4-1. Combinational subsystems sample descriptions
  * Lesson 4-2. BCD-7 segments code converter (exercise)
  * Lesson 4.3. Parametrized bin/gray converters
  * Lesson 4-4. Subsystems-based circuit analysis (exercise)

### Unit 5. Arithmetic circuits

  * Lesson 5-1. Adders design
  * Lesson 5-2. Adder/subtractor design
  * Lesson 5-3. Arithmetic-Logic Unit design

### Unit 6. Sequential circuits

  * Lesson 6-1. Bistable circuits
  * Lesson 6-2. Bistable circuits comparison
  * Lesson 6-3. Blocking and non-blocking assignments
  * Lesson 6-4. Finite-state machines. Sequence detector
  * Lesson 6-5. Arbiter design

### Unit 7. Counters and registers

  * Lesson 7-1. Universal register
  * Lesson 7-2. Multi-opperation counter design
  * Lesson 7-3. Exercise: digital chronometer design

### Unit 8. Memories

  * Lesson 8-1. ROM-based multiplier design
  * Lesson 8-2. Asynchronous RAM
  * Lesson 8-3. Synchronous RAM

## CONVENTIONS

  * All files use 4-space code indentation. Lines are a maximum of 80
   characters long to make all files easy to read and edit in a 80 columns terminal.

  * The code uses two types of comments with different functions. Type `//...`
    comments are regular brief code comments. Type `/*...*/` comments contains
    longer explanaitions of concepts and use of the Verilog language and are
    the core of the course. These long comments can be deleted from the code
    without affecting its clarity so that the code in the examples could be
    used in your own projects. Long comments can be easily removed from a file
    like in the following example:

        $ awk '/\/\*/,/\*\// {next};{print}' alarm.v > clean/alarm.v

  * This course uses the default Verilog standard that Icarus Verilog uses. It
    is Verilog-2005 (IEEE 1634-2005) at the time of writing. Most examples, if
    not all, will work with any simulador supporting the Verilog-2001 standard,
    though. Any modern Verilog toolchain should support this version at least.

  * The code uses ANSI-style contructions throughout the course (similar to
    ANSI-C) introduced by the Verilog-2001 standard.

## ADDITIONAL EXAMPLES

You can find additional examples and practical Verilog designs at the author's
project [verilog-examples](https://gitlab.com/jjchico/verilog-examples).

## CONTRIBUTIONS

Contributions to this project are welcome in any format: bug reports, comments,
merge requests, etc.

## AUTHOR

2009-2019 Jorge Juan-Chico <jjchico@gmail.com>

## LICENSE

This file is part of curso_verilog.v. curso_verilog.v is free software: you can
redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
See <http://www.gnu.org/licenses/>.