// Design:      addsub
// File:        addsub.v
// Description: Two's complement adder-subtractors
// Author:      Jorge Juan <jjchico@gmail.com>
// Date:        22-11-2013 (initial version)

/* Lesson 5.2. Adder-subtractors

   In this lesson we will see two versions of a two's complement
   adder-subtractor circuit using arithmetic operators, in a similar way to 
   what we did in the previous lesson.

   This lesson also introduces a very important concept in hardware description
   languages: signed signals. The use of Verilog 'parameters' is also
   revisited in this lesson in order to desing modules with an arbitrary
   data width and we introduce the definition of local variables in named
   code blocks.
 */

//////////////////////////////////////////////////////////////////////////
// Two's complement adder-subtractor version 1                          //
//////////////////////////////////////////////////////////////////////////

/* This description uses arithmetic operators. The operation is controlled by
 * input 'op' (0-add, 1-subtract) and the module includes an overflow signal
 * 'ov' that activates ('1') when the result of the operation cannot be 
 * represented with the bits available at the output.
 *
 * The data width of the module is not fixed but indicated with the parameter
 * WIDTH. The default for the parameter value is 8 as indicated by the 
 * 'parameter' directive, but can be changed when the module is instantiated
 * as we have already seen, so the same code can produce adder/subtractors of
 * any data width. In general, the more characteristics of a module are
 * parametrized, more flexible and reusable the module is, so the designer must
 * pat attention and use parameters whenever it is possible.
 *
 * Inputs 'a' and 'b' and output 'f' are of type 'signed' so that the simulator
 * and synthesizer will assume two's complement notation and arithmetic, 
 * displacement and sigh extensions will behave accordingly, together with the
 * display format of '$display'. */

module addsub1 #(
    parameter WIDTH = 8                 // number of bits
    )(
    input wire signed [WIDTH-1:0] a,    // first operand
    input wire signed [WIDTH-1:0] b,    // second operand
    input wire op,                      // operation (0-add, 1-subtract)
    output reg signed [WIDTH-1:0] f,    // output
    output reg ov                       // overflow
    );

    /* Remember: 'f' and 'ov' are declare as type 'reg' because they are
     * goint to be assigned inside a procedural 'always' block. */

    always @* begin :sub
        /* We define a variable local to the 'always' block to hold the
         * intermediate value of the operation with one additional bit. The
         * definition of variables local to a block is only possible if the
         * block has a name, indicated with ':sub' in this case. */
        reg signed [WIDTH:0] s;
        case (op)
          0:
            s = a + b;
          default:
            s = a - b;
        endcase

        // Overflow output
        /* 'f' always holds the correct value of the operation because
         * it has one extra bit. Overflow can be detected if we compare
         * the correct sign bit in f[WIDTH] with the corresponding sign bit
         * of the WIDTH bits representation (f[WIDTH-1]). */
        if (s[WIDTH] != s[WIDTH-1])
            ov = 1;
        else
            ov = 0;

        // Output
        f = s[WIDTH-1:0];
    end

endmodule // addsub1

//////////////////////////////////////////////////////////////////////////
// Two's complement adder-subtractor version 2                          //
//////////////////////////////////////////////////////////////////////////

/* In the addsub1 description we have used signed variables and we do 'a + b'
 * or 'a - b' at convenience. Depending on how smart the synthesis tool is
 * it may infer a single adder-subtractor block or independent adder and
 * subtractor with a greater cost. In the following alternative description
 * we use unsigned variables so the two's complement representation has to be
 * handled by the designer directly. It means that in case we want to subtract
 * we will have to add the one's complement of 'b' plus one. The overflow bit
 * will also have to be calculated based on the operands and the result. This 
 * version of the adder-subtractor is thus done at a lower level and it 
 * requires the designer to understand the details of the representation and 
 * make an additional effort to write the code to handle these details. In
 * general, a designer may check the tool's manual and do some test to check
 * which design would give better implementation results. The original 
 * Verilog 1995 standard did not include signed variables, that were
 * introduced in the 2001 standard of the language.  */

module addsub2 #(
    parameter WIDTH = 8
    )(
    input wire [WIDTH-1:0] a,  // first operand
    input wire [WIDTH-1:0] b,  // second operand
    input wire op,             // operation (0-suma, 1-resta)
    output reg [WIDTH-1:0] f,  // output
    output reg ov              // overflow
    );

    always @* begin :sub
        /* Auxiliary variable to calculate the second operand of the adder. */
        reg [WIDTH-1:0] c;

        /* We use a conditional assignment to calculate 'c', which is a very
         * compact alternative to 'if'. In case we want to subtract, 'c' is
         * assigned with the one's complement of 'b'. */
        c = op == 0 ? b : ~b;

        /* Result calculation. We add 'op' to obtain the two's complement of
         * 'b' in case of a subtraction. */
        f = a + c + op;

        // Overflow output
        /* Overflow only occurs when we ... */
        ov = ~a[WIDTH-1] & ~c[WIDTH-1] & f[WIDTH-1] |
             a[WIDTH-1] & c[WIDTH-1] & ~f[WIDTH-1];
    end

endmodule // addsub2

/*
   EXERCISES

   1. Compile the example with:

      $ iverilog addsub.v

      and check that there are no syntax errors.

   (continues in 'addsub_tb.v')
*/
