// Design:      addsub
// File:        addsub.v
// Description: Two's complement adder-subtractors. Test bench.
// Author:      Jorge Juan <jjchico@gmail.com>
// Date:        22-11-2013 (initial version)

/* Lesson 5.2. Adder-subtractors
   
   This file contains a test bench for mofules 'addsub1' and 'addsub2'.
 */

`timescale 1ns / 1ps

// This test bench applies a configurable number of random input patterns to
// the circuit under test.
// Simulation macros and default values:
//   NP: number of test patterns
//   SEED: initial seed for pseudo-random number generation
//   OP: type of operation (OP=0 -> add, OP=1 -> sub)

`ifndef NP
    `define NP 20
`endif
`ifndef SEED
    `define SEED 1
`endif
`ifndef OP
    `define OP 0
`endif

module test ();

    reg signed [7:0] a;     // input 'a'
    reg signed [7:0] b;     // input 'b'
    reg op = `OP;           // type of operation (0-add, 1-sub)
    wire signed [7:0] f;    // output
    wire ov;                // overflow output
    integer np;             // number of patterns (auxiliary signal)
    integer seed = `SEED;   // seed (auxiliary signal)

    // Circuit under test
    /* Modules 'addsub1' and 'addsub2' have a parameterized data width. Here
     * we instantiate 8-bit modules. You can substitute 'addsub1' by
     * 'addsub2' to simulate the alternative implementation. */
    addsub1 #(.WIDTH(8)) uut(.a(a), .b(b), .op(op), .f(f), .ov(ov));

    initial begin
        /* 'np' is a counter that holds the remaining number of patterns
         * to be applied. The initial value is taken from macro NP. */
        np = `NP;
        
        // Waveform generation (optional)
        $dumpfile("addsub_tb.vcd");
        $dumpvars(0, test);
        
        // Output printing
        if (op == 0)
            $display("Operation: ADD");
        else
            $display("Operation: SUBTRACT");	
        $display("   A    B     f  ov");
        $display("-------------------");
        $monitor("%d %d  %d  %b",
                   a, b, f, ov);
    end

    // Test generation process
    /* Each 20ns 'a', 'b' and 'cin' are assigned random values. Simulation
     * ends after applying NP patterns. The pattern sequence can be 
     * changed by defining a different value for SEED. Macro values NP and SEED
     * can be changed at the top of this file or using compilation options. */
    always begin
        #20
        a = $random(seed);
        b = $random(seed);
        np = np - 1;
        if (np == 0)
            $finish;
    end
endmodule

/*
   EXERCISES

   2. Compile the test bench with:

        $ iverilog addsub.v addsub_tb.v

      and test its operation with:

        $ vvp a.out

      Take a look at the output text and waveforms (with Gtkwave) and check 
      that the operation is correct.
      
      Also check the subtract opration with:
      
        $ iverilog -DOP=1 addsub.v addsub_tb.v
      
   3. Repeat the simulation with different values of OP, NP and SEED and check
      the results. For example:
      
        $ iverilog -DOP=1 -DNP=40 -DSEED=2 addsub.v addsub_tb.v
        $ vpp a.out

   4. Modify the test bench to simulate module 'addsub2' and repeat the
      previous exercises.

   5. Modify the test bench so that it can simulate and 'addsub1' module of
      any number of bits, defined by a macro 'WIDTH'. Try it by simulating
      and 16-bit adder subtractor like:

        $ iverilog -DWIDTH=16 addsub.v addsub_tb.v
*/
