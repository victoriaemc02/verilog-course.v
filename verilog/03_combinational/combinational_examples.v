// Diseño:      ejemplos
// Archivo:     ejemplos_combinacionales.v
// Descripción: Varios ejemplos combinacionales en Verilog
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       09/11/2009 (versión inicial)

/*
   Unidad 3: Ejemplos combinacionales

   En esta unidad veremos algunos ejemplos de resolución de problemas y
   aplicaciones de Verilog con circuitos combinacionales. Con los ejemplos
   se introducirán algunos conceptos nuevos de Verilog y algunas alternativas
   a las formas de descripción y realización de bancos de pruebas vistos en
   lecciones anteriores.

   Lección 3.1 (alarma.v): diseño de un sencillo sistema de alarma para un
   automovil.

   Lección 3.2 (primos.v): diseño de un detector de números primos para
   números de 4 bits.

   Lección 3.3 (azar.v): diseño y simulación de un circuito simple con azares.
   Permite comprobar cómo la introducción de retrasos puede producir
   resultados transitorios no esperados en los circuitos combinacionales.
*/

module ejemplos_combinacionales ();

    initial
        $ display("Ejemplos combinacionales.");

endmodule
